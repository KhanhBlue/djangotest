from django.shortcuts import render
from .form import RegistrationFrom
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    return render(request,'pages/home.html')

def contact(request):
    return render(request,'pages/contact.html')

def error(request):
    return render(request,'pages/404.html')

def register(request):
    form = RegistrationFrom()
    if request.method == 'POST':
        form = RegistrationFrom(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/home')
    return render(request,'pages/register.html',{'form':form})
