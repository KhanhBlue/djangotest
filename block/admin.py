from django.contrib import admin
from .models import post

# Register your models here.
class postAdmin(admin.ModelAdmin):
    list_display=['title','date']
    list_filter=['date']
    search_fields=['title']
admin.site.register(post,postAdmin)
