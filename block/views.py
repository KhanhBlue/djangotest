from django.shortcuts import render, get_object_or_404
from .models import post,Comment
from django.views.generic import ListView, DetailView
from block.forms import CommentForm
from django.http import HttpResponseRedirect

# Create your views here.
# def list(request):
#     data =  {'posts': post.objects.all().order_by("-date")}
#     return render(request, 'block/block.html',data)

class PostListView(ListView):
    queryset = post.objects.all().order_by("-date") # sap xep theo thoi gian 
    template_name = 'block/block.html' # link den html
    context_object_name = 'posts'
    paginate_by = 1 # so trang cua 1 bai viet

# def postid(request, id):
#     postid = {'postid':post.objects.get(id=id)}
#     return render(request,'block/postid.html',postid)

class PostDetailView(DetailView):
    model = post
    template_name = 'block/postid.html'

